﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(estacionamientoUSAM.Startup))]
namespace estacionamientoUSAM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
