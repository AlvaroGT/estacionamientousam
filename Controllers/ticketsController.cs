﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using estacionamientoUSAM.Datos;

namespace estacionamientoUSAM.Controllers
{
    public class ticketsController : Controller
    {
        private estacionamientoEntities db = new estacionamientoEntities();

        // GET: tickets
        public async Task<ActionResult> Index()
        {
            var ticket = db.ticket.Include(t => t.estacionamiento).Include(t => t.tarifa).Include(t => t.vehiculo);
            return View(await ticket.ToListAsync());
        }

        // GET: tickets/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ticket ticket = await db.ticket.FindAsync(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // GET: tickets/Create
        public ActionResult Create()
        {
            ViewBag.idestacionamiento = new SelectList(db.estacionamiento, "idestacionamiento", "locacion");
            ViewBag.idtarifa = new SelectList(db.tarifa, "idtarifa", "nombre");
            ViewBag.fk_idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo");
            return View();
        }

        // POST: tickets/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idticket,horaentrada,horasalida,estado,idestacionamiento,fk_idplaca,idtarifa")] ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.ticket.Add(ticket);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idestacionamiento = new SelectList(db.estacionamiento, "idestacionamiento", "locacion", ticket.idestacionamiento);
            ViewBag.idtarifa = new SelectList(db.tarifa, "idtarifa", "nombre", ticket.idtarifa);
            ViewBag.fk_idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo", ticket.fk_idplaca);
            return View(ticket);
        }

        // GET: tickets/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ticket ticket = await db.ticket.FindAsync(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            ViewBag.idestacionamiento = new SelectList(db.estacionamiento, "idestacionamiento", "locacion", ticket.idestacionamiento);
            ViewBag.idtarifa = new SelectList(db.tarifa, "idtarifa", "nombre", ticket.idtarifa);
            ViewBag.fk_idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo", ticket.fk_idplaca);
            return View(ticket);
        }

        // POST: tickets/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idticket,horaentrada,horasalida,estado,idestacionamiento,fk_idplaca,idtarifa")] ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idestacionamiento = new SelectList(db.estacionamiento, "idestacionamiento", "locacion", ticket.idestacionamiento);
            ViewBag.idtarifa = new SelectList(db.tarifa, "idtarifa", "nombre", ticket.idtarifa);
            ViewBag.fk_idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo", ticket.fk_idplaca);
            return View(ticket);
        }

        // GET: tickets/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ticket ticket = await db.ticket.FindAsync(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ticket ticket = await db.ticket.FindAsync(id);
            db.ticket.Remove(ticket);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
