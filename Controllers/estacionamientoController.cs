﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using estacionamientoUSAM.Datos;

namespace estacionamientoUSAM.Controllers
{
    public class estacionamientoController : Controller
    {
        private estacionamientoEntities db = new estacionamientoEntities();

        // GET: estacionamiento
        public async Task<ActionResult> Index()
        {
            return View(await db.estacionamiento.ToListAsync());
        }

        // GET: estacionamiento/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estacionamiento estacionamiento = await db.estacionamiento.FindAsync(id);
            if (estacionamiento == null)
            {
                return HttpNotFound();
            }
            return View(estacionamiento);
        }

        // GET: estacionamiento/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: estacionamiento/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idestacionamiento,locacion,cantidadA,cantidadM")] estacionamiento estacionamiento)
        {
            if (ModelState.IsValid)
            {
                db.estacionamiento.Add(estacionamiento);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(estacionamiento);
        }

        // GET: estacionamiento/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estacionamiento estacionamiento = await db.estacionamiento.FindAsync(id);
            if (estacionamiento == null)
            {
                return HttpNotFound();
            }
            return View(estacionamiento);
        }

        // POST: estacionamiento/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idestacionamiento,locacion,cantidadA,cantidadM")] estacionamiento estacionamiento)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estacionamiento).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(estacionamiento);
        }

        // GET: estacionamiento/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            estacionamiento estacionamiento = await db.estacionamiento.FindAsync(id);
            if (estacionamiento == null)
            {
                return HttpNotFound();
            }
            return View(estacionamiento);
        }

        // POST: estacionamiento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            estacionamiento estacionamiento = await db.estacionamiento.FindAsync(id);
            db.estacionamiento.Remove(estacionamiento);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
