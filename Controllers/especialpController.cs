﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using estacionamientoUSAM.Datos;

namespace estacionamientoUSAM.Controllers
{
    public class especialpController : Controller
    {
        private estacionamientoEntities db = new estacionamientoEntities();

        // GET: especialp
        public async Task<ActionResult> Index()
        {
            var especialp = db.especialp.Include(e => e.vehiculo);
            return View(await especialp.ToListAsync());
        }

        // GET: especialp/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            especialp especialp = await db.especialp.FindAsync(id);
            if (especialp == null)
            {
                return HttpNotFound();
            }
            return View(especialp);
        }

        // GET: especialp/Create
        public ActionResult Create()
        {
            ViewBag.idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo");
            return View();
        }

        // POST: especialp/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idespecialp,nombre,cargo,carnet,idplaca")] especialp especialp)
        {
            if (ModelState.IsValid)
            {
                db.especialp.Add(especialp);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo", especialp.idplaca);
            return View(especialp);
        }

        // GET: especialp/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            especialp especialp = await db.especialp.FindAsync(id);
            if (especialp == null)
            {
                return HttpNotFound();
            }
            ViewBag.idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo", especialp.idplaca);
            return View(especialp);
        }

        // POST: especialp/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idespecialp,nombre,cargo,carnet,idplaca")] especialp especialp)
        {
            if (ModelState.IsValid)
            {
                db.Entry(especialp).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idplaca = new SelectList(db.vehiculo, "idplaca", "tipovehiculo", especialp.idplaca);
            return View(especialp);
        }

        // GET: especialp/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            especialp especialp = await db.especialp.FindAsync(id);
            if (especialp == null)
            {
                return HttpNotFound();
            }
            return View(especialp);
        }

        // POST: especialp/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            especialp especialp = await db.especialp.FindAsync(id);
            db.especialp.Remove(especialp);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
