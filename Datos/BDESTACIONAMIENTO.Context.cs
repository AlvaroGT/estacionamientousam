﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace estacionamientoUSAM.Datos
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class estacionamientoEntities : DbContext
    {
        public estacionamientoEntities()
            : base("name=estacionamientoEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<empleado> empleado { get; set; }
        public virtual DbSet<especialp> especialp { get; set; }
        public virtual DbSet<estacionamiento> estacionamiento { get; set; }
        public virtual DbSet<tarifa> tarifa { get; set; }
        public virtual DbSet<ticket> ticket { get; set; }
        public virtual DbSet<vehiculo> vehiculo { get; set; }
    }
}
